<?php
/**
 * Plugin Name: Gourmet Artist Post Types
 * Plugin URI:
 * Description: Adds Custom Post Types to Gourmet Artist Site
 * Version: 1.0
 * Author: SetCain
 * Author URI:
 * License: GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */

 function create_post_type_recetas() {
     register_post_type('recetas', array(
         'labels' => array(
             'name' => __('Recetas'),
            //  'singular_name' => __('Receta')
         ),
         'public' => true,
         'has_archive' => true
         ));
 }

 add_action('init', 'create_post_type_recetas');

?>